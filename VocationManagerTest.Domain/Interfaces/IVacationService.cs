﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using VocationManagerTest.Domain.Model.Vacation;

namespace VocationManagerTest.Domain.Interfaces
{
    public interface IVacationService
    {
        Task<List<VacationListItem>> GetList(VacationListParameters parameters, int userId);
        Task<List<int>> GetYears();
        Task<List<EditVacationDetails>> GetDetailsList(int userId);
        Task<EditVacationDetails> GetEditDetails(int userId);
        Task Save(EditVacationDetails editVacationDetails);
        Task Remove(int id);
    }
}
