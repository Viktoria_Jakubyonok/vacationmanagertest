﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VocationManagerTest.Domain.Model;
using VocationManagerTest.Domain.Model.Department;

namespace VocationManagerTest.Domain.Interfaces
{
    public interface IDepartmentService
    {
        Task<List<NamedEntity>> GetNames(int userId);
        Task<List<DepartmentListItem>> GetList();
        Task<EditDepartmentDetails> GetEditDetails(int id);
        Task Remove(int id);
        Task RemoveEmployee(int employeeId, int departmentId);
        Task Save(EditDepartmentDetails editDepartmentDetails);
    }
}
