﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VocationManagerTest.Database;
using VocationManagerTest.Database.Enums;
using VocationManagerTest.Database.Model;
using VocationManagerTest.Domain.Interfaces;
using VocationManagerTest.Domain.Model;
using VocationManagerTest.Domain.Model.Department;
using VocationManagerTest.Domain.Model.Employee;

namespace VocationManagerTest.Domain.Services
{
    public class DepartmentService : IDepartmentService
    {
        private readonly ApplicationDbContext _db;

        public DepartmentService(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<List<NamedEntity>> GetNames(int userId)
        {
            var role = _db.Employees.Where(e => e.Id == userId).Select(e => e.Role)
                .FirstOrDefault();

            if (role != EmployeeRole.Head)
            {
                return await _db.Departments.Select(d => new NamedEntity
                {
                    Id = d.Id,
                    Name = d.Name
                }).ToListAsync();
            }
            else
            {
                return await _db.EmployeeDepartments.Where(e => e.EmployeeId == userId)
                    .Select(d => new NamedEntity
                    {
                        Id = d.DepartmentId,
                        Name = d.Department.Name
                    }).ToListAsync();
            }
        }

        public async Task<List<DepartmentListItem>> GetList()
        {
            var departmentEmployeeCount = await _db.EmployeeDepartments
                .GroupBy(ed => new {ed.DepartmentId, DepartmentName = ed.Department.Name}).Select(g =>
                    new DepartmentListItem
                    {
                        Id = g.Key.DepartmentId,
                        Name = g.Key.DepartmentName,
                        CountMember = g.Count(),
                        Heads = g.Where(ed=>ed.Employee.Role==EmployeeRole.Head&&ed.DepartmentId==g.Key.DepartmentId).Select(e=>e.Employee.SurName).ToList()
                    }).ToListAsync();

            return departmentEmployeeCount;
        }

        public async Task<EditDepartmentDetails> GetEditDetails(int id)
        {
            return await _db.Departments.Select(d => new EditDepartmentDetails
            {
                Id = d.Id,
                Name = d.Name,
                HeadIds = _db.EmployeeDepartments.Where(ed=>ed.DepartmentId==d.Id&&ed.Employee.Role==EmployeeRole.Head).Select(ed=>ed.EmployeeId).ToList()
            }).FirstOrDefaultAsync(d => d.Id == id);
        }

        public async Task RemoveEmployee(int employeeId, int departmentId)
        {
            var departmentEmployee = await _db.EmployeeDepartments.FirstOrDefaultAsync(ed =>
                ed.DepartmentId == departmentId && ed.EmployeeId == employeeId);

            if (departmentEmployee != null)
            {
                _db.EmployeeDepartments.Remove(departmentEmployee);
                await _db.SaveChangesAsync();
            }
        }

        public async Task Remove(int departmentId)
        {
            var department = await _db.Departments.FindAsync(departmentId);
            if (department != null)
            {
                _db.Departments.Remove(department);
            }

            var employee = await _db.EmployeeDepartments
                .Where(ed => ed.DepartmentId == departmentId && ed.Employee.Role != EmployeeRole.Head)
                .Select(e=>e.Employee).ToListAsync();
            if (employee != null)
            {
                foreach (var e in employee)
                {
                    _db.Employees.Remove(e);
                }
            }

            await _db.SaveChangesAsync();
        }

        public async Task Save(EditDepartmentDetails editDepartmentDetails)
        {

            var isNew = editDepartmentDetails.Id == 0;
            var department = isNew ? new Department() : await _db.Departments.FindAsync(editDepartmentDetails.Id);

            department.Name = editDepartmentDetails.Name;
            if (isNew) _db.Departments.Add(department);
            else _db.Departments.Update(department);

            var existingHeads = isNew
                ? new List<EmployeeDepartment>(0)
                : await _db.EmployeeDepartments
                    .Where(ed => ed.DepartmentId == department.Id && ed.Employee.Role == EmployeeRole.Head)
                    .ToListAsync();

            var existingHeadIds = existingHeads.Select(e => e.EmployeeId).ToList();

            var addedEmployeeIds = editDepartmentDetails.HeadIds.Except(existingHeadIds).ToList();
            foreach (var addedEmployeeId in addedEmployeeIds)
            {
                _db.EmployeeDepartments.Add(new EmployeeDepartment
                {
                    DepartmentId = department.Id,
                    EmployeeId = addedEmployeeId,
                    CreationDate = DateTime.UtcNow
                });
            }

            var removedEmployeeIds = existingHeadIds.Except(editDepartmentDetails.HeadIds).ToList();
            foreach (var removedEmployeeId in removedEmployeeIds)
            {
                var removedHead = existingHeads.First(e => e.EmployeeId == removedEmployeeId);
                _db.EmployeeDepartments.Remove(removedHead);
            }

            await _db.SaveChangesAsync();
        }
    }
}
