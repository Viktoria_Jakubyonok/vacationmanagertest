﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VocationManagerTest.Database;
using VocationManagerTest.Database.Enums;
using VocationManagerTest.Database.Model;
using VocationManagerTest.Domain.Model;
using VocationManagerTest.Domain.Model.Employee;
using VocationManagerTest.Shared;

namespace VocationManagerTest.Domain.Services
{
    public class EmployeeService
    {
        private readonly ApplicationDbContext _db;

        public EmployeeService(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<List<NamedEntity>> GetNameHeads()
        {
            return await _db.Employees.Where(e=>e.Role==EmployeeRole.Head).Select(e => new NamedEntity
            {
                Id = e.Id,
                Name = e.Email
            }).ToListAsync();
        }

        public async Task<List<NamedEntity>> GetNameFreeEmployees()
        {
            return await _db.Employees.Select(e => new NamedEntity
            {
                Id = e.Id,
                Name = e.Email
            }).ToListAsync();
        }


        public async Task<List<EmployeeListItem>> GetList(EmployeeListParameters parameters, int userId)
        {
            var result = new List<EmployeeListItem>();

            var departmentNames = _db.EmployeeDepartments.Where(e => e.EmployeeId == userId)
                    .Select(d => d.Department.Name).ToList();
            var role = _db.Employees.Where(e => e.Id == userId).Select(e => e.Role)
                .FirstOrDefault();

            IQueryable<EmployeeDepartment> query = _db.EmployeeDepartments;

            if (parameters.DepartmentId.HasValue)
            {
                query  = query.Where(ed => ed.DepartmentId == parameters.DepartmentId);
            }

            if (parameters.IsOnlyEmployees)
            {
                query = query.Where(ed => ed.Employee.Role != EmployeeRole.Head);
            }
            
            var employeeDepartments = await query.Select(e => new EmployeeListItem
            {
                Id = e.Employee.Id,
                Email = e.Employee.Email,
                Name = e.Employee.Name,
                SurName = e.Employee.SurName,
                Role = e.Employee.Role,
                DepartmentNames  = new List<string>(){e.Department.Name},
            }).ToListAsync();

            if (role != EmployeeRole.Admin)
            {
               var intersect = await query.Select(ed => ed.Department.Name).Intersect(departmentNames).ToListAsync();
               foreach (var item in employeeDepartments)
                {
                    if (intersect.Contains(item.DepartmentNames.FirstOrDefault()))
                    {
                        result.Add(item);
                    }
                }
            }
            else
            {
                result = employeeDepartments;
            }

            return result.GroupBy(ed => ed.Id).Select(g => new EmployeeListItem
            {
                Id = g.First().Id,
                Email = g.First().Email,
                Name = g.First().Name,
                SurName = g.First().SurName,
                Role = g.First().Role,
                DepartmentNames = g.Select(d => d.DepartmentNames.FirstOrDefault()).ToList()
            }).ToList();
        }

        public async Task<EditEmployeeDetails> GetEditDetails(int id)
        {
            return await _db.Employees.Select(e => new EditEmployeeDetails
            {
                Id = e.Id,
                Email = e.Email,
                Name = e.Name,
                Surname = e.SurName,
                Role = e.Role,
                DepartmentIds = _db.EmployeeDepartments.Where(ed => ed.EmployeeId == e.Id).Select(ed => ed.DepartmentId).ToList()
            }).FirstOrDefaultAsync(e => e.Id == id);
        }

      

        public async Task Remove(int employeeId)
        {
            var employee = await _db.Employees.FindAsync(employeeId);
            if (employee != null)
            {
                _db.Employees.Remove(employee);
                await _db.SaveChangesAsync();
            }
        }

        public async Task Save(EditEmployeeDetails editEmployeeDetails)
        {

            var existingWithSameName = await _db.Employees.FirstOrDefaultAsync(e => e.Email == editEmployeeDetails.Email && e.Id != editEmployeeDetails.Id);
            if (existingWithSameName != null)
            {
                throw new Exception("Пользователь с такими данными уже существует.");
            }

            var isNew = editEmployeeDetails.Id == 0;

            var employee = isNew ? new Employee() : await _db.Employees.FindAsync(editEmployeeDetails.Id);

            employee.Email = editEmployeeDetails.Email;
            employee.Name = editEmployeeDetails.Name;
            employee.SurName = editEmployeeDetails.Surname;
            employee.Role = editEmployeeDetails.Role;

            if (!string.IsNullOrWhiteSpace(editEmployeeDetails.Password))
            {
                employee.Password = Hash.PasswordHash(editEmployeeDetails.Password);
            }

            if (isNew) _db.Employees.Add(employee);
            else _db.Employees.Update(employee);

            var existingDepartments = isNew
                ? new List<EmployeeDepartment>(0)
                : await _db.EmployeeDepartments.Where(e => e.EmployeeId == employee.Id).ToListAsync();

            var existingDepartmentIds = existingDepartments.Select(d => d.DepartmentId).ToList();

            var addedDepartmentIds = editEmployeeDetails.DepartmentIds.Except(existingDepartmentIds).ToList();
            foreach (var addedDepartmentId in addedDepartmentIds)
            {
                _db.EmployeeDepartments.Add(new EmployeeDepartment
                {
                    EmployeeId = employee.Id,
                    DepartmentId = addedDepartmentId,
                    CreationDate = DateTime.UtcNow
                });
            }

            var removedDepartmentIds = existingDepartmentIds.Except(editEmployeeDetails.DepartmentIds).ToList();
            foreach (var removedDepartmentId in removedDepartmentIds)
            {
                var removedDepartment = existingDepartments.First(d => d.DepartmentId == removedDepartmentId);
                _db.EmployeeDepartments.Remove(removedDepartment);
            }

            await _db.SaveChangesAsync();
        }
    }
}
