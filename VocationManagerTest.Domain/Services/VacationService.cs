﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VocationManagerTest.Database;
using VocationManagerTest.Database.Enums;
using VocationManagerTest.Database.Model;
using VocationManagerTest.Domain.Interfaces;
using VocationManagerTest.Domain.Model;
using VocationManagerTest.Domain.Model.Vacation;
using VocationManagerTest.Shared.Extensions;
using VocationManagerTest.Shared;

namespace VocationManagerTest.Domain.Services
{
    public class VacationService: IVacationService
    {
        private readonly ApplicationDbContext _db;

        public VacationService(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<List<int>> GetYears()
        {
            var minYear = await _db.Vacations.Select(v => v.BeginningDate.Year).MinAsync();
            var maxYear = await _db.Vacations.Select(v => v.ExpirationDate.Year).MaxAsync();

            return Enumerable.Range(minYear, maxYear - minYear + 1).ToList();
        }

        public async Task<List<VacationListItem>> GetList(VacationListParameters parameters, int userId)
        {
          IQueryable<Vacation> query = _db.Vacations;
            var year = DateTime.Now.Year;

            if (parameters.DepartmentIds.HasItems() && parameters.Year.HasValue)
            {

                var employeeIds = _db.EmployeeDepartments.Where(e =>
                    parameters.DepartmentIds.Contains(e.DepartmentId)).Select(e => e.EmployeeId).Distinct();

                query = query.Where(v => employeeIds.Contains(v.EmployeeId)&&(parameters.Year==v.BeginningDate.Year||v.ExpirationDate.Year==parameters.Year));
                year = parameters.Year.Value;
            }
            else
            {
                query = query.Where(v => v.BeginningDate.Year == DateTime.Now.Year||v.ExpirationDate.Year==DateTime.Now.Year);
            }

            var beginningDateLessCurrent = query
                    .Where(v => v.BeginningDate.Year < year)
                    .Select(
                        v => new
                        {
                            Surname = v.Employee.SurName,
                            EmployeeId = v.EmployeeId,
                            BeginningDate = new DateTime(year, Constants.FirstMonthOfYear, Constants.FirstDayOfYear),
                            ExpirationDate = v.ExpirationDate
                        });

            var endingDateMoreCurrent = query
                    .Where(v => v.ExpirationDate.Year > year)
                    .Select(
                        v => new
                        {
                            Surname = v.Employee.SurName,
                            EmployeeId = v.EmployeeId,
                            BeginningDate = v.BeginningDate,
                            ExpirationDate = new DateTime(year, Constants.LastMonthOfYear, Constants.LastDayOfYear)
                        });

            var endingEqualsBeginning = query
                .Where(v => v.ExpirationDate.Year == year&&v.BeginningDate.Year==year)
                .Select(
                    v => new
                    {
                        Surname = v.Employee.SurName,
                        EmployeeId = v.EmployeeId,
                        BeginningDate = v.BeginningDate,
                        ExpirationDate = v.ExpirationDate
                    });

            var vacationDateList = beginningDateLessCurrent.Concat(endingDateMoreCurrent).Concat(endingEqualsBeginning)
                .ToList();


            var vacation = vacationDateList.Select(v => new VacationListItem
            {
                Surname = v.Surname,
                EmployeeId = v.EmployeeId,
                CountOfDay = new List<int>() {(v.ExpirationDate.Date - v.BeginningDate.Date).Days},
                NumberOfWeeks = WeekList(v.BeginningDate, v.ExpirationDate)
            }).ToList();

            var result = vacation.GroupBy(e => e.EmployeeId).Select(g => new VacationListItem
            { 
                EmployeeId = g.First().EmployeeId,
                Surname = g.First().Surname,
                CountOfDay = g.Select(cd=>cd.CountOfDay.FirstOrDefault()).ToList(),
                NumberOfWeeks = g.SelectMany(nw=>nw.NumberOfWeeks).ToList()
            }).ToList();
            
            return result;
        }

        public async Task<List<EditVacationDetails>> GetDetailsList(int userId)
        {
            return await _db.Vacations
                .Where(v => v.EmployeeId == userId).Select(v => new EditVacationDetails
                {
                    Id = v.Id,
                    EmployeeId = userId,
                    Surname = v.Employee.SurName,
                    BeginningDate = v.BeginningDate,
                    ExpirationDate = v.ExpirationDate,
                }).ToListAsync();
        }

        public async Task<EditVacationDetails> GetEditDetails(int id)
        {
            return await _db.Vacations.Select(v => new EditVacationDetails
            {
                Id = v.Id,
                EmployeeId = v.EmployeeId,
                Surname = v.Employee.SurName,
                BeginningDate = v.BeginningDate,
                ExpirationDate = v.ExpirationDate,
            }).FirstOrDefaultAsync(v=>v.Id==id);
        }

        public async Task Remove(int vacationId)
        {
            var vacation = await _db.Vacations.FindAsync(vacationId);
            if (vacation != null)
            {
                _db.Vacations.Remove(vacation);
                await _db.SaveChangesAsync();
            }

        }

            public async Task Save(EditVacationDetails editVacationDetails)
        {

            var isNew = editVacationDetails.Id == 0;
            var vacation = isNew ? new Vacation() : await _db.Vacations.FindAsync(editVacationDetails.Id);

            vacation.BeginningDate = editVacationDetails.BeginningDate;
            vacation.ExpirationDate = editVacationDetails.ExpirationDate;
            if (isNew) vacation.CreationDate = DateTime.Now;
            vacation.LastEditDate = DateTime.Now;
            vacation.EmployeeId = editVacationDetails.EmployeeId;

            if (isNew) _db.Vacations.Add(vacation);
            else _db.Vacations.Update(vacation);

            await _db.SaveChangesAsync();
        }

        public int WeekNumber(DateTime fromDate)
        {
            return CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(fromDate, CalendarWeekRule.FirstDay,
                DayOfWeek.Monday);
        }

        public List<int> WeekList(DateTime start, DateTime end)
        {
            var startWeekNumber = WeekNumber(start);
            var result = new List<int>();
            var endWeekNumber = WeekNumber(end);

            if (startWeekNumber > endWeekNumber)
            {
                startWeekNumber = 0;
            }
           
            var subtraction = endWeekNumber - startWeekNumber;
            result.Add(startWeekNumber);

            for (var i = 0; i<subtraction; i++)
            {
                startWeekNumber+=1;
                result.Add(startWeekNumber);
            }

            return result;

        }
    }
}
