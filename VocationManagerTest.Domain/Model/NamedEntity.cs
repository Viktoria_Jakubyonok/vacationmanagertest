﻿

namespace VocationManagerTest.Domain.Model
{
    public class NamedEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
