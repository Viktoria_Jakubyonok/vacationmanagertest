﻿using System;
using System.Collections.Generic;
using System.Text;
using VocationManagerTest.Database.Enums;
using VocationManagerTest.Database.Model;

namespace VocationManagerTest.Domain.Model.Employee
{
    public class EmployeeListItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SurName { get; set; }
        public string Email { get; set; }
        public EmployeeRole Role { get; set; }
        public List<string> DepartmentNames { get; set; }
      
    }

    public class EmployeeListParameters
    {
        public int? DepartmentId { get; set; }

        public bool IsOnlyEmployees { get; set; }
    }
}
