﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using VocationManagerTest.Database.Enums;
using VocationManagerTest.Database.Model;

namespace VocationManagerTest.Domain.Model.Employee
{
    public class EditEmployeeDetails
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Не указано Имя")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Не указана Фамилия")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Не указан Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage ="Не указана роль сотрудника")]
        public EmployeeRole Role { get; set; }

        [Required(ErrorMessage = "Не указан отдел сотрудника")]
        public List<int> DepartmentIds  { get; set; }
    }
}
