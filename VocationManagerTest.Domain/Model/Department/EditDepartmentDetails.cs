﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VocationManagerTest.Domain.Model.Department
{
    public class EditDepartmentDetails
    {
        [Required]
        public int Id { get; set; }
        [Required(ErrorMessage = "Не указано Имя")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Не указан глава отдела")]
        public List<int> HeadIds { get; set; }
        public List<int> EmployeeIds { get; set; }
    }
}
