﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VocationManagerTest.Domain.Model.Department
{
    public class DepartmentListItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<string> Heads { get; set; }
        public int CountMember { get; set; }
    }
}
