﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VocationManagerTest.Domain.Model.Vacation
{
    public class VacationListItem
    {
        public int Id { get; set; }
        public List<int> NumberOfWeeks { get; set; }
        public string Surname { get; set; }
        public int EmployeeId { get; set; }
        public List<int> CountOfDay { get; set; }
    }

    public class VacationListParameters
    {
        public List<int> DepartmentIds { get; set; }
        public int? Year { get; set; }
    }
}
