﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VocationManagerTest.Domain.Model.Vacation
{
    public class EditVacationDetails
    {
        public int Id { get; set; }
        public string Surname { get; set; }
        public int EmployeeId { get; set; }
        [Required(ErrorMessage = "Не указана дата начала отпуска")]
        [DataType(DataType.Date)]
        public DateTime BeginningDate { get; set; }
        [Required(ErrorMessage = "Не указана дата окончания отпуска")]
        [DataType(DataType.Date)]
        public DateTime ExpirationDate { get; set; }

    }
}
