﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VocationManagerTest.Database.Model
{
    public class Vacation
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }

        public DateTime BeginningDate{ get; set; }
        public DateTime ExpirationDate { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastEditDate { get; set; }



    }
}
