﻿using System;

namespace VocationManagerTest.Database.Model
{
    public class EmployeeDepartment
    {
        public int DepartmentId { get; set; }
        public Department Department { get; set; }

        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
