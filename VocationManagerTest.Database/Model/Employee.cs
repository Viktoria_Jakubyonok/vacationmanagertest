﻿using System.Collections.Generic;
using VocationManagerTest.Database.Enums;

namespace VocationManagerTest.Database.Model
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SurName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public EmployeeRole Role { get; set; }

        public ICollection<EmployeeDepartment> EmployeeDepartments { get; set; }
        public ICollection<Vacation> Vacations { get; set; }
    }
}
