﻿using System.Collections.Generic;

namespace VocationManagerTest.Database.Model
{
    public class Department
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<EmployeeDepartment> EmployeeDepartments { get; set; }
    }
}
