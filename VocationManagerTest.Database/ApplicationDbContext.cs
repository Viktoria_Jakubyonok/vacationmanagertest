﻿using Microsoft.EntityFrameworkCore;
using System;
using VocationManagerTest.Database.Enums;
using VocationManagerTest.Database.Model;
using VocationManagerTest.Shared;

namespace VocationManagerTest.Database
{
    public class ApplicationDbContext: DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
          : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeeDepartment> EmployeeDepartments { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Vacation> Vacations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            string adminEmail = "admin@mail.ru";
            string adminPassword = "123456";//123456
            string adminName = "admin";
            string adminSurname = "admin";

            Department department = new Department { Id = 1, Name = "Отдел_1" };
            Employee adminEmployee = new Employee { Id = 1, Name = adminName, SurName = adminSurname, Email = adminEmail, Password = Hash.PasswordHash(adminPassword), Role = EmployeeRole.Admin };
            Employee headEmployee = new Employee
            {
                Id = 2,
                Name = "Иван",
                SurName = "Иванов",
                Email = "ivanov@mail.com",
                Password = Hash.PasswordHash("ivanov"),//ivanov
                Role = EmployeeRole.Head
            };
            EmployeeDepartment employeeDepartment = new EmployeeDepartment { EmployeeId = headEmployee.Id, DepartmentId = department.Id, CreationDate = DateTime.Now };

            modelBuilder.Entity<Department>().HasData(new Department[] { department });
            modelBuilder.Entity<Employee>().HasData(new Employee[] { adminEmployee, headEmployee });
            modelBuilder.Entity<EmployeeDepartment>().HasKey(table => new { table.DepartmentId, table.EmployeeId });
            modelBuilder.Entity<EmployeeDepartment>().HasData(new EmployeeDepartment[] { employeeDepartment });

            base.OnModelCreating(modelBuilder);

        }
    }
}
