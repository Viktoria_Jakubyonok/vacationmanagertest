﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using VocationManagerTest.Database;

namespace VocationManagerTest.Database.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20190606145616_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("VocationManagerTest.Database.Model.Department", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Departments");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Name = "Отдел_1"
                        });
                });

            modelBuilder.Entity("VocationManagerTest.Database.Model.Employee", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Email");

                    b.Property<string>("Name");

                    b.Property<string>("Password");

                    b.Property<byte>("Role");

                    b.Property<string>("SurName");

                    b.HasKey("Id");

                    b.ToTable("Employees");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Email = "admin@mail.ru",
                            Name = "admin",
                            Password = "ce0bfd15059b68d67688884d7a3d3e8c",
                            Role = (byte)1,
                            SurName = "admin"
                        },
                        new
                        {
                            Id = 2,
                            Email = "ivanov@mail.com",
                            Name = "Иван",
                            Password = "dd8d1fde410502c414c813d2874d6758",
                            Role = (byte)2,
                            SurName = "Иванов"
                        });
                });

            modelBuilder.Entity("VocationManagerTest.Database.Model.EmployeeDepartment", b =>
                {
                    b.Property<int?>("DepartmentId");

                    b.Property<int?>("EmployeeId");

                    b.Property<DateTime>("CreationDate");

                    b.HasKey("DepartmentId", "EmployeeId");

                    b.HasIndex("EmployeeId");

                    b.ToTable("EmployeeDepartments");

                    b.HasData(
                        new
                        {
                            DepartmentId = 1,
                            EmployeeId = 2,
                            CreationDate = new DateTime(2019, 6, 6, 17, 56, 16, 704, DateTimeKind.Local).AddTicks(3462)
                        });
                });

            modelBuilder.Entity("VocationManagerTest.Database.Model.EmployeeDepartment", b =>
                {
                    b.HasOne("VocationManagerTest.Database.Model.Department", "Department")
                        .WithMany()
                        .HasForeignKey("DepartmentId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("VocationManagerTest.Database.Model.Employee", "Employee")
                        .WithMany()
                        .HasForeignKey("EmployeeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
