﻿namespace VocationManagerTest.Database.Enums
{
    public enum EmployeeRole : byte
    {
        Admin = 1,
        Head = 2,
        User = 3
    }
}
