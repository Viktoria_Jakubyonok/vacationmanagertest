﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using VocationManagerTest.Domain.Model;

namespace VocationManagerTest.Web.Extension
{
    public static class ViewExtensions
    {
        public static string ToYesNo(this bool b)
        {
            return b ? "Yes" : "No";
        }

        public static List<SelectListItem> ToSelectListItems(this IEnumerable<NamedEntity> items)
        {
            return items.Select(i => new SelectListItem { Value = i.Id.ToString(), Text = i.Name }).ToList();
        }

        public static List<SelectListItem> ToSelectListItems<T>(this IEnumerable<T> items)
        {
            return items.Select(i => new SelectListItem { Value = i.ToString(), Text = i.ToString() }).ToList();
        }
    }
}
