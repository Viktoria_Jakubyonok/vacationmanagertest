﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VocationManagerTest.Database;
using VocationManagerTest.Database.Model;
using VocationManagerTest.Domain.Interfaces;
using VocationManagerTest.Domain.Model;
using VocationManagerTest.Domain.Model.Employee;
using VocationManagerTest.Domain.Services;
using VocationManagerTest.Shared;
using VocationManagerTest.Web.ViewModels;
using ControllerBase = VocationManagerTest.Web.Controllers.Base.ControllerBase;

namespace VocationManagerTest.Web.Controllers
{
    [Authorize]
    public class EmployeeController : ControllerBase
    {
        private EmployeeService _employeeService;
        private readonly IDepartmentService _departmentService;

        public EmployeeController(EmployeeService employeeService, IDepartmentService departmentService)
        {
            _employeeService = employeeService;
            _departmentService = departmentService;
        }

        public async Task<ActionResult> Index(EmployeeListParameters parameters)
        {
            var model = new EmployeeListViewModel
            {
                Parameters = parameters,
                EmployeeList = await _employeeService.GetList(parameters, UserId),
                AvailableDepartments = await _departmentService.GetNames(UserId)
            };
            

            return View("EmployeeList", model);
        }

        public async Task<ActionResult> Remove(int id)
        {
            await _employeeService.Remove(id);
            return RedirectToAction("Index");
        }


        [HttpGet]
        public async Task<ActionResult> Create(int? departmentId)
        {
            var employee = new EditEmployeeDetails();

            if (departmentId.HasValue)
            {
                employee.DepartmentIds = new List<int> { departmentId.Value };
            }

            return await EditView(employee);

        }

        [HttpPost]
        public async Task<ActionResult> Create(EditEmployeeDetails employee)
        {
            return await Save(employee);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            var editEmployeeDetails = await _employeeService.GetEditDetails(id);
            if (editEmployeeDetails == null)
            {
                return RedirectToAction("Index");
            }

            return await EditView(editEmployeeDetails);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(EditEmployeeDetails employee)
        {
            return await Save(employee);
        }


        public async Task<ActionResult> Save(EditEmployeeDetails employee)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _employeeService.Save(employee);
                    return RedirectToAction("Index");
                }
                catch(Exception e)
                {
                    ModelState.AddModelError(string.Empty, e.Message);
                }
            }
            return await EditView(employee);
        }

        private async Task<ActionResult> EditView(EditEmployeeDetails employee)
        {
            var model = new EditEmployeeViewModel
            {
                Employee = employee,
                AvailableDepartments = await _departmentService.GetNames(UserId)
            };

            return View("EditEmployee", model);
        }

     }
}