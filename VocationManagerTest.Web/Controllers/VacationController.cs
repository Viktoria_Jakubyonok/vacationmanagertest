﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VocationManagerTest.Database.Enums;
using VocationManagerTest.Domain.Interfaces;
using VocationManagerTest.Domain.Model.Vacation;
using VocationManagerTest.Shared.Extensions;
using VocationManagerTest.Web.ViewModels;
using ControllerBase = VocationManagerTest.Web.Controllers.Base.ControllerBase;

namespace VocationManagerTest.Web.Controllers
{
    [Authorize]
    public class VacationController : ControllerBase
    {
        private readonly IVacationService _vacationService;
        private readonly IDepartmentService _departmentService;

        public VacationController(IVacationService vacationService, IDepartmentService departmentService)
        {
            _vacationService = vacationService;
            _departmentService = departmentService;
        }

        [Authorize(Roles = "Admin, Head")]
        public async Task<IActionResult> Index(VacationListParameters parameters)
        {
            if (!parameters.DepartmentIds.HasItems() && User.IsInRole(EmployeeRole.Head.ToString()))
            {
                parameters.DepartmentIds = (await _departmentService.GetNames(UserId)).Select(d => d.Id).ToList();
            }

            var model = new VacationListViewModel
            {
                Parameters = parameters,
                VacationList = await _vacationService.GetList(parameters, UserId),
                AvailableYear =  await _vacationService.GetYears(),
                AvailableDepartments = await _departmentService.GetNames(UserId),
            };

            return View("VacationList", model);
        }

        public async Task<ActionResult> ShowVacation(int? id)
        {
            var userId = 0;
            if (User.IsInRole(EmployeeRole.User.ToString()) || (!User.IsInRole(EmployeeRole.User.ToString()) && !id.HasValue))
            {
                userId = UserId;
            }
            else
            {
                if(id.HasValue)
                userId = id.Value;
            }
            var model = await _vacationService.GetDetailsList(userId);
            return View("VacationDetailsList", model);
        }

        [HttpGet]
        public async Task<ActionResult> Create(int? id)
        {
            var userId = 0;
            if (User.IsInRole(EmployeeRole.User.ToString())||(!User.IsInRole(EmployeeRole.User.ToString())&&!id.HasValue))
            {
                userId = UserId;
            }
            else
            {
                if (id.HasValue)
                    userId = id.Value;
            }
            var model = new EditVacationDetails(){EmployeeId = userId};
            return await EditView(model);

        }
        
        [HttpPost]
        public async Task<ActionResult> Create(EditVacationDetails vacation)
        {
            return await Save(vacation);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            var editVacationDetails = await _vacationService.GetEditDetails(id);
            if (editVacationDetails == null)
            {
                return RedirectToAction("Index");
            }

            return await EditView(editVacationDetails);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(EditVacationDetails vacation)
        {
            return await Save(vacation);
        }

        public async Task<ActionResult> Remove(int id)
        {
            await _vacationService.Remove(id);
            return RedirectToAction("ShowVacation");
        }


        public async Task<ActionResult> Save(EditVacationDetails vacation)
        {
            if (ModelState.IsValid && vacation.BeginningDate >= DateTime.Now &&
                                          vacation.BeginningDate <= vacation.ExpirationDate)
            {
                try
                {
                    await _vacationService.Save(vacation);
                    return RedirectToAction("ShowVacation");
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(string.Empty, e.Message);
                }
            }
            else
            {
                return RedirectToAction("Create");
            }
            return await EditView(vacation);
        }

        private async Task<ActionResult> EditView(EditVacationDetails vacation)
        {

            var model = new EditVacationViewModel
            {
                Vacation = vacation
            };

            return View("EditVacation", model);
        }




    }
}