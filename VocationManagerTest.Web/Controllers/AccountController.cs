﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VocationManagerTest.Database;
using VocationManagerTest.Database.Model;
using VocationManagerTest.Domain.Model;
using VocationManagerTest.Shared;
using VocationManagerTest.Domain.Services;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using VocationManagerTest.Database.Enums;

namespace VocationManagerTest.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly ApplicationDbContext _db;

        public AccountController(ApplicationDbContext db)
        {
            _db=db;
        }

        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                Employee employee = await _db.Employees
                    .FirstOrDefaultAsync( e => e.Email==model.Email && e.Password == Shared.Hash.PasswordHash(model.Password));
                if (employee!= null)
                {
                    await Authenticate(employee);

                    if (employee.Role!=EmployeeRole.User)
                    {
                        return RedirectToAction("Index", "Employee");
                    }
                    else
                    {
                        return RedirectToAction("ShowVacation", "Vacation");
                    }
                }

                ModelState.AddModelError("", "Неверный логин и(или) пароль");
            }

            return View(model);
        }

        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index", "Account");
        }

        private async Task Authenticate(Employee employee)
        {
            var claims = new List<Claim>
            {
                new Claim(Constants.UserIdClaimName,  employee.Id.ToString()),
                new Claim(ClaimsIdentity.DefaultNameClaimType,  employee.Email.ToString()),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, employee.Role.ToString())
            };

            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }
    }
}