﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.SignalR;
using VocationManagerTest.Domain.Interfaces;
using VocationManagerTest.Domain.Model.Department;
using VocationManagerTest.Domain.Model.Employee;
using VocationManagerTest.Domain.Services;
using VocationManagerTest.Web.ViewModels;
using ControllerBase = VocationManagerTest.Web.Controllers.Base.ControllerBase;


namespace VocationManagerTest.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class DepartmentController: ControllerBase
    {
        private readonly IDepartmentService _departmentService;
        private EmployeeService _employeeService;

        public DepartmentController(IDepartmentService departmentService, EmployeeService employeeService)
        {
            _departmentService = departmentService;
            _employeeService = employeeService;
        }

        public async Task<ActionResult> Index()
        {
            var model = await _departmentService.GetList();
            
            return  View("DepartmentList", model);
        }

        [HttpGet]
        public async Task<ActionResult> Create()
        {
            return await EditView(new EditDepartmentDetails());
        }

        [HttpPost]
        public async Task<ActionResult> Create(EditDepartmentDetails department)
        {
            return await Save(department);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            var editDepartmentDetails = await _departmentService.GetEditDetails(id);
            if (editDepartmentDetails == null)
            {
                return RedirectToAction("Index");
            }

            return await EditView(editDepartmentDetails);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(EditDepartmentDetails department)
        {
            return await Save(department);
        }

        public async Task<ActionResult> RemoveEmployee(int employeeId, int departmentId)
        {
            await _departmentService.RemoveEmployee(employeeId, departmentId);
            //TODO
            return RedirectToAction("Edit", "Department", new { Id = departmentId });
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Remove(int id)
        {
            await _departmentService.Remove(id);
            return RedirectToAction("Index");
        }

       
        public async Task<ActionResult> Save(EditDepartmentDetails department)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _departmentService.Save(department);
                    return RedirectToAction("Edit");
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(string.Empty, e.Message);
                }
            }
            return await EditView(department);
        }

        private async Task<ActionResult> EditView(EditDepartmentDetails department)
        {
            var parameters = new EmployeeListParameters
            {
                DepartmentId = department.Id,
                IsOnlyEmployees = true
            };

            var model = new EditDepartmentViewModel
            {
                Department = department,
                AvailableHead = await _employeeService.GetNameHeads(),
                AvailableEmployees = await _employeeService.GetNameFreeEmployees(),
                Parameters = parameters,
                EmployeeList = await _employeeService.GetList(parameters,UserId)
            };

            return View("EditDepartment", model);
        }
    }
}
