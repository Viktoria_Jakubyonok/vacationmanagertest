﻿using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using VocationManagerTest.Shared;

namespace VocationManagerTest.Web.Controllers.Base
{
    public abstract class ControllerBase : Controller
    {
        private int? _userId;
        public int UserId
        {
            get
            {
                if (!_userId.HasValue)
                {
                    if (User.Identity.IsAuthenticated)
                    {
                        _userId = int.Parse(((ClaimsIdentity) User.Identity).FindFirst(Constants.UserIdClaimName).Value);
                    }
                }

                return _userId ?? 0;
            }
        }
    }
}