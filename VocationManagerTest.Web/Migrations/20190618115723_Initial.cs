﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VocationManagerTest.Web.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "EmployeeDepartments",
                keyColumns: new[] { "DepartmentId", "EmployeeId" },
                keyValues: new object[] { 1, 2 },
                column: "CreationDate",
                value: new DateTime(2019, 6, 18, 14, 57, 23, 440, DateTimeKind.Local).AddTicks(9544));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "EmployeeDepartments",
                keyColumns: new[] { "DepartmentId", "EmployeeId" },
                keyValues: new object[] { 1, 2 },
                column: "CreationDate",
                value: new DateTime(2019, 6, 18, 14, 54, 42, 779, DateTimeKind.Local).AddTicks(8372));
        }
    }
}
