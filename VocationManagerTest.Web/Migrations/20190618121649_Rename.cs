﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VocationManagerTest.Web.Migrations
{
    public partial class Rename : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "lastEditDate",
                table: "Vacations",
                newName: "LastEditDate");

            migrationBuilder.RenameColumn(
                name: "expirationDate",
                table: "Vacations",
                newName: "ExpirationDate");

            migrationBuilder.RenameColumn(
                name: "creationDate",
                table: "Vacations",
                newName: "CreationDate");

            migrationBuilder.RenameColumn(
                name: "beginningDate",
                table: "Vacations",
                newName: "BeginningDate");

            migrationBuilder.UpdateData(
                table: "EmployeeDepartments",
                keyColumns: new[] { "DepartmentId", "EmployeeId" },
                keyValues: new object[] { 1, 2 },
                column: "CreationDate",
                value: new DateTime(2019, 6, 18, 15, 16, 48, 781, DateTimeKind.Local).AddTicks(6854));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "LastEditDate",
                table: "Vacations",
                newName: "lastEditDate");

            migrationBuilder.RenameColumn(
                name: "ExpirationDate",
                table: "Vacations",
                newName: "expirationDate");

            migrationBuilder.RenameColumn(
                name: "CreationDate",
                table: "Vacations",
                newName: "creationDate");

            migrationBuilder.RenameColumn(
                name: "BeginningDate",
                table: "Vacations",
                newName: "beginningDate");

            migrationBuilder.UpdateData(
                table: "EmployeeDepartments",
                keyColumns: new[] { "DepartmentId", "EmployeeId" },
                keyValues: new object[] { 1, 2 },
                column: "CreationDate",
                value: new DateTime(2019, 6, 18, 14, 57, 23, 440, DateTimeKind.Local).AddTicks(9544));
        }
    }
}
