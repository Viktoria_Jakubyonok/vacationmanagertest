﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VocationManagerTest.Domain.Model;
using VocationManagerTest.Domain.Model.Employee;

namespace VocationManagerTest.Web.ViewModels
{
    public class EmployeeListViewModel
    {
        public EmployeeListParameters Parameters { get; set; }
        public List<EmployeeListItem> EmployeeList { get; set; }
        public List<NamedEntity> AvailableDepartments { get; set; }
    }


}
