﻿using System.Collections.Generic;
using VocationManagerTest.Domain.Model;
using VocationManagerTest.Domain.Model.Employee;

namespace VocationManagerTest.Web.ViewModels
{
    public class EditEmployeeViewModel
    {
        public EditEmployeeDetails Employee { get; set; }
        public List<NamedEntity> AvailableDepartments { get; set; }
    }
}
