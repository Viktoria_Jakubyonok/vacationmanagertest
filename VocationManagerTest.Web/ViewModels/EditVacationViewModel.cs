﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VocationManagerTest.Domain.Model.Vacation;

namespace VocationManagerTest.Web.ViewModels
{
    public class EditVacationViewModel
    {
        public EditVacationDetails Vacation { get; set; }
    }
}
