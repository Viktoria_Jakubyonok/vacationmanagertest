﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VocationManagerTest.Domain.Model;
using VocationManagerTest.Domain.Model.Department;
using VocationManagerTest.Domain.Model.Employee;

namespace VocationManagerTest.Web.ViewModels
{
    public class EditDepartmentViewModel
    {
        public EditDepartmentDetails Department { get; set; }
        public List<NamedEntity> AvailableHead { get; set; }
        public List<NamedEntity> AvailableEmployees { get; set; }

        public EmployeeListParameters Parameters { get; set; }
        public List<EmployeeListItem> EmployeeList { get; set; }
    }
}
