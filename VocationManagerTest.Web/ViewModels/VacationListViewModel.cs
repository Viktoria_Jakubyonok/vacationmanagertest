﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VocationManagerTest.Domain.Model;
using VocationManagerTest.Domain.Model.Employee;
using VocationManagerTest.Domain.Model.Vacation;

namespace VocationManagerTest.Web.ViewModels
{
    public class VacationListViewModel
    {
        public VacationListParameters Parameters { get; set; }
        public List<VacationListItem> VacationList { get; set; }
        public List<NamedEntity> AvailableDepartments { get; set; }
        public List<int> AvailableYear { get; set; }

    }
}
