﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VocationManagerTest.Shared
{
    public class Constants
    {
        public const string UserIdClaimName = "userid";
        public const int CountWeekOfYear = 53;
        public const int FirstDayOfYear = 1;
        public const int FirstMonthOfYear = 1;
        public const int LastDayOfYear = 31;
        public const int LastMonthOfYear = 12;

    }
}
