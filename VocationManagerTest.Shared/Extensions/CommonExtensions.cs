﻿using System.Collections.Generic;
using System.Linq;

namespace VocationManagerTest.Shared.Extensions
{
    public static class CommonExtensions
    {
        public static bool HasItems<T>(this IEnumerable<T> items)
        {
            return items != null && items.Any();
        }
    }
}
